# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.2]
### Removed
- All files not needed for witnesses to run the software

### Moved
- All files needed for development to the [Hotbox Dev repo](https://gitlab.com/jrswab/hotbox-dev)

## [1.0.1] - 2018-11-30
### Changed
- share-file-size in config.ini.example to 8G
- readme to reflect intrustional changes and updates.

## [1.0.0] - 2018-09-30
### Added
- SMOKE 0.0.5
- Config template with node IPs included
- Escape the docker container with `ctrl+h`